﻿using Engine.Common.Events;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Engine.Components;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    [SerializeField]
    private GameObject _heartPrefab;
    [SerializeField]
    private Transform _healthTransform;
    [SerializeField]
    private Text _killCounterText;
    [SerializeField]
    private GameObject _loseWindow;

    private List<GameObject> _hpCounter;
    private int _killCount;

    private void InitUI() {
        _killCount = 0;
        _killCounterText.text = _killCount.ToString();
        _hpCounter = new List<GameObject>();
    }

    private void Awake() {
        EventManager.Instance.AddEventListener<HealthComponentEvent>(OnHealthComponentEvent);
        EventManager.Instance.AddEventListener<LevelObjectEvent>(OnLevelObjectEvent);
        InitUI();
    }

    private void OnDisable() {
        EventManager.Instance?.RemoveEventListener<HealthComponentEvent>(OnHealthComponentEvent);
        EventManager.Instance?.RemoveEventListener<LevelObjectEvent>(OnLevelObjectEvent);
    }

    private void OnLevelObjectEvent(LevelObjectEvent levelObjectEvent) {
        if (levelObjectEvent.Target.ObjectType != LevelObjectType.Enemy) return;
        if (levelObjectEvent.EventSubtype != LevelObjectEventType.Dead) return;
        _killCount++;
        _killCounterText.text = _killCount.ToString();
    }

    private void OnHealthComponentEvent(HealthComponentEvent healthComponentEvent) {
        if (healthComponentEvent.Target.LevelObject.ObjectType != LevelObjectType.Player) return;
        if (healthComponentEvent.EventSubtype != HealthComponentEventType.HealthChanged) return;
        UpdateHealthBar(healthComponentEvent.Target.Health);
        if (healthComponentEvent.Target.Health == 0) GameOverWindow();
    }

    private void GameOverWindow() {
        StartCoroutine(GameOver());
        _loseWindow.SetActive(true);
    }

    private IEnumerator GameOver() {
        Time.timeScale = 0;
        yield return new WaitForSecondsRealtime(3);
        SceneManager.LoadScene("Menu");
    }

    private void UpdateHealthBar(int currentHealthCount) {
        var healthDif = currentHealthCount - _hpCounter.Count;
        if (healthDif > 0) {
            for (int i = 0; i < healthDif; i++) {
                var heart = Instantiate(_heartPrefab, _healthTransform);
                _hpCounter.Add(heart);
            }
        }
        else {
            Destroy(_hpCounter[0]);
            _hpCounter.Remove(_hpCounter[0]);
        }
    }
}
