﻿using System;
using System.Collections;
using Engine.Common.Events;
using UnityEngine;

namespace Engine.Components {
    public class ShootingComponent : BaseComponent {
        [SerializeField]
        private GameObject _shellPrefab;
        [SerializeField]
        private float _cooldownTime;
        [SerializeField]
        private Transform _shootPosition;

        private bool _canShoot;
        private Vector3 _shootOffset;

        protected override void Init() {
            EventManager.Instance.AddEventListener<InputComponentEvent>(OnInputComponentEvent);
            _canShoot = true;
        }

        private void OnDisable() {
            EventManager.Instance?.RemoveEventListener<InputComponentEvent>(OnInputComponentEvent);
        }
        private void OnInputComponentEvent(InputComponentEvent inputComponentEvent) {
            if (!_canShoot) return;
            if (inputComponentEvent.Target.LevelObject != LevelObject) return;
            Instantiate(_shellPrefab, _shootPosition.position, gameObject.transform.rotation);
            _canShoot = false;
            StartCoroutine(Cooldown());
        }

        private IEnumerator Cooldown() {
            yield return new WaitForSeconds(_cooldownTime);
            _canShoot = true;
        }
    }
}