﻿using Engine.Components;
using UnityEngine;

public class CameraControler : MonoBehaviour {
    private Vector3 heightFix;
    private Vector3 playerPos;
    private Vector3 cameraPos;
    private Transform _playerTransform;

    public void Start() {
        heightFix = new Vector3(0, 0, -10);
        _playerTransform = FindObjectOfType<PlayerComponent>().GetComponent<RunnerComponent>().transform;
    }

    public void Update() {
        playerPos = _playerTransform.position;
        cameraPos = transform.position;

        if (playerPos.x >= -2.4f && playerPos.x <= 2.4f && playerPos.y >= -3.6f && playerPos.y <= 3.6f) {
            transform.position = playerPos + heightFix;
        }
        else if (playerPos.x < -2.4f) {
            if (playerPos.y >= -3.6f && playerPos.y <= 3.6f)
                transform.position = new Vector3(-2.4f, playerPos.y, 0) + heightFix;
        }
        if (playerPos.x > 2.4f) {
            if (playerPos.y >= -3.6f && playerPos.y <= 3.6f)
                transform.position = new Vector3(2.4f, playerPos.y, 0) + heightFix;
        }
        if (playerPos.y < -3.6f) {
            if (playerPos.x >= -2.4f && playerPos.x <= 2.4f)
                transform.position = new Vector3(playerPos.x, -3.6f, 0) + heightFix;
        }
        if (playerPos.y > 3.6f) {
            if (playerPos.x >= -2.4f && playerPos.x <= 2.4f)
                transform.position = new Vector3(playerPos.x, 3.6f, 0) + heightFix;
        }
    }
}
