﻿using Engine.Common.Events;
using Engine.Components;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {
    [SerializeField]
    private GameObject _enemyPrefab;
    [SerializeField]
    private List<Transform> _spawnPoints;

    private List<LevelObject> _spawnedEnemy;
    private List<LevelObject> _disabledEnemy;
    private Coroutine _spawner;

    private void Start() {
        _spawnedEnemy = new List<LevelObject>();
        _disabledEnemy = new List<LevelObject>();
        _spawner = StartCoroutine(Spawner());
        EventManager.Instance.AddEventListener<LevelObjectEvent>(OnLevelObjectEvent);
        EventManager.Instance.AddEventListener<EnemyAIEvent>(OnEnemyAIEvent);
    }

    private void OnEnemyAIEvent(EnemyAIEvent enemyAIEvent) {
        if (enemyAIEvent.EventSubtype != EnemyAIEventType.RunAway) return;
        if (!_spawnedEnemy.Contains(enemyAIEvent.Target.LevelObject)) return;
        DestroyEnemy(enemyAIEvent.Target.LevelObject);
    }

    private void OnLevelObjectEvent(LevelObjectEvent levelObjectEvent) {
        if (levelObjectEvent.EventSubtype != LevelObjectEventType.Disabled) return;
        if (!_spawnedEnemy.Contains(levelObjectEvent.Target)) return;
        DestroyEnemy(levelObjectEvent.Target);
    }

    private void DestroyEnemy(LevelObject levelObject) {
        _spawnedEnemy.Remove(levelObject);
        _disabledEnemy.Add(levelObject);
    }

    private void OnDisable() {
        if (_spawner != null)
            StopCoroutine(_spawner);
    }

    private IEnumerator Spawner() {
        var delay = new WaitForSeconds(2f);
        while (true) {
            var randomTransform = GetRandomPosition();
            GameObject enemy;
            if (_disabledEnemy.Count == 0)
                enemy = Instantiate(_enemyPrefab, randomTransform.position, randomTransform.rotation * Quaternion.Euler(GetRandomRotation()));
            else {
                enemy = _disabledEnemy[0].gameObject;
                _disabledEnemy.Remove(_disabledEnemy[0]);
                enemy.SetActive(true);
                enemy.transform.position = randomTransform.position;
                enemy.transform.rotation = randomTransform.rotation * Quaternion.Euler(GetRandomRotation());
            }
            _spawnedEnemy.Add(enemy.GetComponent<LevelObject>());
            yield return delay;
        }
    }

    private Transform GetRandomPosition() {
        return _spawnPoints[Random.Range(0, _spawnPoints.Count)];
    }

    private Vector3 GetRandomRotation() {
        return new Vector3(0, 0, Random.Range(-60, 60));
    }
}
